let collection = [];

// Write the queue functions below.
function print() {
  return collection;
}

function enqueue(element) {
  collection[collection.length] = element;
  return [...new Set(collection)];
}

function dequeue() {
  let newCollection = [];
  for (let i = 0; i < collection.length - 1; i++) {
    newCollection[i] = collection[i + 1];
  }
  collection = newCollection;
  return [...new Set(collection)];
}

function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  if (collection.length === 0) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
